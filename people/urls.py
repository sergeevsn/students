from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter

from people.views import NewStudentsView

router = DefaultRouter()
router.register(r'students', NewStudentsView, basename='user')
urlpatterns = router.urls