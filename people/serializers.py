from rest_framework import  serializers

from people.models import Student


# class StudentsSerializer(serializers.Serializer):
#     name = serializers.CharField(max_length=2000)
#     age = serializers.IntegerField()
#     description = serializers.CharField(max_length=2000)
#     level = serializers.IntegerField()
#
#     def create(self, validated_data):
#         return Student.objects.create(**validated_data)
#
#     def update(self, instance, validated_data):
#         instance.name = validated_data.get('name', instance.name)
#         instance.age = validated_data.get('age', instance.age)
#         instance.description = validated_data.get('description', instance.description)
#         instance.level = validated_data.get('level', instance.level)
#
#         instance.save()
#         return instance

class NewStudentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ('id', 'name', 'age', 'description', 'level', 'unversity_id')