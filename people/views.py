from rest_framework import viewsets

from people.models import Student
from people.serializers import NewStudentsSerializer


# Create your views here.

# class StudentsView(APIView):
#     def get(self, request):
#         students = Student.objects.all()
#         serializer = StudentsSerializer(students, many=True)
#         return Response({"students": serializer.data})
#
#     def post(self, request):
#         student = request.data.get('students')
#
#         serializer = StudentsSerializer(data=student)
#
#         if serializer.is_valid(raise_exception=True):
#             student_saved = serializer.save()
#
#         return Response({'success': "Student created {}".format(student_saved.name)})
#
#     def put(self, request, name):
#         saved_student = get_object_or_404(Student.objects.all(), pk=pk)

#
#         data = request.data.get('students')
#         serializer  = StudentsSerializer(instance=saved_student, data=data, partial=True)
#
#         if serializer.is_valid(raise_exception=True):
#             saved_student = serializer.save()
#         return Response({'success': "Student updated {}".format(saved_student.name)})
#
#     def delete(self, request, pk):
#         to_delete_student = get_object_or_404(Student.objects.all(), pk=pk)
#         to_delete_student_name = to_delete_student.name
#         to_delete_student.delete()
#         return Response({'success': "Student {} deleted".format(to_delete_student_name)})


# class NewStudentsView(CreateAPIView):
#
#     queryset = Student.objects.all()
#     serializer_class = NewStudentsSerializer
#
#     def get(self, request, *args, **kwargs):
#         return self.list(request, *args, **kwargs)
#     def post(self, request, *args, **kwargs):
#         return self.create(request, *args, **kwargs)
#
#     def perform_create(self, serializer):
#         unversity = get_object_or_404(University.objects.all(), id=self.request.data.get('university_id'))
#         return serializer.save(unversity=unversity)

class NewStudentsView(viewsets.ModelViewSet):
    serializer_class = NewStudentsSerializer
    queryset = Student.objects.all()