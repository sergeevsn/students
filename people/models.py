

from django.db import models
from django.utils import timezone


# Create your models here.

class University(models.Model):
    name = models.CharField(max_length=2000)

    def __str__(self):
        return self.name


class Student(models.Model):
    name = models.CharField(max_length=2000)
    age = models.IntegerField()
    description = models.TextField(max_length=2000)
    level = models.IntegerField(default=0)
    unversity = models.ForeignKey(University, on_delete=models.CASCADE, null=True, related_name='students')

    created_date = models.DateField(default=timezone.now)

    def __str__(self):
        return self.name
